# CS371p: Object-Oriented Programming Allocator Repo

* Name: Steven Callahan

* EID: svc434

* GitLab ID: @Zebo616

* HackerRank ID: @stevencallahan

* Git SHA: c185e0d258e3b431432f878713ab391efff56fcd

* GitLab Pipelines: https://gitlab.com/Zebo616/cs371p-allocator/-/pipelines

* Estimated completion time: 6 hours

* Actual completion time: 24 hours

* Comments: Kept making dumb mistakes that cost me hours of debugging; for the next project I'm definitely going to find a partner.
