// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        friend class my_allocator;
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    public:
        // ----
        // data
        // ----

        int* _p;

        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        // MY CODE
        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        // MY CODE
        iterator& operator ++ () {
            char* castedP = reinterpret_cast<char*>(_p);
            castedP += abs(*reinterpret_cast<int*>(castedP)) + 8;
            _p = reinterpret_cast<int*>(castedP);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int inc) {
            iterator x = *this;
            for(int i = 0; i < inc; i++) {
                ++*this;
            }
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* castedP = reinterpret_cast<char*>(_p);
            castedP -= abs(*reinterpret_cast<int*>(castedP - 4)) - 8;
            _p = reinterpret_cast<int*>(castedP);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int inc) {
            iterator x = *this;
            for(int i = 0; i < inc; i++) {
                --*this;
            }
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        friend class my_allocator;
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return reinterpret_cast<const int&>(*_p);
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            const char* castedP = reinterpret_cast<const char*>(_p);
            castedP += abs(*reinterpret_cast<const int*>(castedP)) + 8;
            _p = reinterpret_cast<const int*>(castedP);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int inc) {
            const_iterator x = *this;
            for(int i = 0; i < inc; ++i) {
                ++*this;
            }
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            char* castedP = reinterpret_cast<const char*>(_p);
            castedP -= abs(*reinterpret_cast<const int*>(castedP - 4)) - 8;
            _p = reinterpret_cast<int*>(castedP);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int inc) {
            const_iterator x = *this;
            for(int i = 0; i < inc; i++) {
                --*this;
            }
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];
    bool print = false;

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Loops through each block of the allocated memory and checks the following:
     * 1. Each sentinel pair holds the same value
     * 2. The summation of the sentinels and their stored values equals the total allocated space
     * 3. No two free blocks are adjacent.
     * valid relies on the iterator working properly.
     */
    bool valid () const {
        using namespace std;
        if(print) {
            printf("checking validity...\n");
        }
        long unsigned int sum = 0;
        const_iterator iter = begin();
        const char* next = reinterpret_cast<const char*>(iter._p);
        if(print) {
            printf("initial next value at %d: %d\n", next, *reinterpret_cast<const int*>(next));
        }
        next += abs(*reinterpret_cast<const int*>(next)) + 4;
        if(print) {
            printf("new next value at %d: %d\n", next, *reinterpret_cast<const int*>(next));
        }
        if(print) {
            printf("using iterator at location %d\n", *iter);
        }
        while(sum < N) {
            if(print) {
                printf("curr sum: %lu\n", sum);
            }
            if(print) {
                printf("iter = %d; next = %d\n", *iter, *reinterpret_cast<const int*>(next));
            }
            // if(sum != 0) {
            //     cout << " ";
            // }
            // cout << *iter << endl;
            sum += 8 + abs(*iter);
            // check that the next sentinel holds the same value as the iterator
            if(print) {
                printf("comparing sentinel values...\n");
            }
            if(*reinterpret_cast<const int*>(next) != *iter) {
                if(print) {
                    printf("Uh Oh! the sentinel pairs don't match! (%d vs %d)\n", *reinterpret_cast<const int*>(next), *iter);
                }
                return false;
            }
            if(print) {
                printf("location in memory: %p vs %p\n", reinterpret_cast<const int*>(next), iter);
            }
            if(print) {
                printf("value: %d vs %d\n", *reinterpret_cast<const int*>(next), *iter);
            }
            if(reinterpret_cast<const int*>(next) != (end()._p) - 1) {
                if(print) {
                    printf("still more blocks to go!\n");
                }
                ++iter;
                next = reinterpret_cast<const char*>(iter._p);
                next += abs(*reinterpret_cast<const int*>(next)) + 4;
            }
        }
        bool isValid = (sum == N);
        if(print) {
            printf("TOTAL SIZE COUNTED IS %d/%d; ISVALID = %d\n", sum, N, isValid);
        }
        return isValid;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // printf("initializing allocator...\n");
        (*this)[0] = N - 8;
        // printf("beginning of allocator initialized to %d\n", (*this)[0]);
        (*this)[N - 4] = N - 8;
        // printf("end of allocator initialized to %d\n", (*this)[N - 4]);
        // printf("initialized.\n");
        // printf("it is valid: %d\n", isValid);
        bool isValid = valid();
        if(print) {
            printf("---my_allocator %d---\n", isValid);
        }
        assert(isValid);
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type rawSize) {
        int size = rawSize * sizeof(value_type);
        if(print) {
            printf("allocating for size %d %lu times for a total of %d bytes\n", sizeof(value_type), rawSize, size);
        }
        iterator iter = begin();
        bool found = false;
        while(!found) {
            assert(iter._p < end()._p);
            // skip occupied blocks of memory
            while(*iter < 0) {
                if(print) {
                    printf("skipping block %d\n", *iter);
                }
                ++iter;
            }
            if(print) {
                printf("found free block; size is %d out of required %lu\n", *iter, size);
            }
            // found a free block; check if it is large enough
            if(std::abs(*iter) >= size) {
                if(print) {
                    printf("free block has sufficient size\n");
                }
                // check to see if we can drop in new sentinels at the end
                if(std::abs(*iter._p) > size + 8) {
                    if(print) {
                        printf("wiggle room; creating new sentinels\n");
                    }
                    // allocate block at the front
                    int oldSize = *iter;
                    if(print) {
                        printf("oldSize = %d\n", oldSize);
                    }
                    char* startOfNewBlock = reinterpret_cast<char*>(iter._p);
                    startOfNewBlock += 8 + size;
                    int* start = reinterpret_cast<int*>(startOfNewBlock);
                    int* endOfOldBlock = start - 1;
                    if(print) {
                        printf("iter = %d, startofnew = %d, endofold = %d\n", iter, reinterpret_cast<int*>(start), endOfOldBlock);
                    }
                    *iter = -size;
                    *endOfOldBlock = -size;
                    // shrink the block at the end
                    *start = oldSize - size - 8;
                    int* endOfNewBlock = reinterpret_cast<int*>(startOfNewBlock + oldSize - size - 4);
                    if(print) {
                        printf("endofnew = %d\n", endOfNewBlock);
                    }
                    *endOfNewBlock = oldSize - size - 8;
                } else {
                    if(print) {
                        printf("tight fit; flipping the block\n");
                    }
                    // not enough room for new sentinels; just flip the block to occupied
                    *iter *= -1;
                    char* other = reinterpret_cast<char*>(iter._p);
                    other += std::abs(*reinterpret_cast<int*>(other)) + 4;
                    int* otherInt = reinterpret_cast<int*>(other);
                    if(print) {
                        printf("other sentinel old value: %d\n", *otherInt);
                    }
                    *otherInt *= -1;
                    if(print) {
                        printf("other sentinel new value: %d\n", *otherInt);
                    }
                }
                found = true;
                break;
            } else {
                if(print) {
                    printf("free block has insufficient size; moving on\n");
                }
            }
            ++iter;
        }
        bool isValid = valid();
        if(print) {
            printf("---allocate %d---\n", isValid);
        }
        assert(isValid);
        pointer thePointer = reinterpret_cast<pointer>(iter._p + 1);
        return thePointer;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        // bool isValid = valid();
        // printf("---construct %d---\n", isValid);
        // assert(isValid);
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * p points to the location in memory returned by the respective allocate call.
     * size should be the bytes to deallocate, but given that p is unchanged from allocate
     * it isn't technically necessary.
     */
    void deallocate (pointer p, size_type rawSize) {
        if(print) {
            printf("deallocate time!\n");
        }
        int* newP = reinterpret_cast<int*>(p);
        int size = rawSize * sizeof(value_type);
        char* startSentinel = reinterpret_cast<char*>(newP - 1);
        char* endSentinel = reinterpret_cast<char*>(newP) + size;
        if(print) {
            printf("deallocating at value %d with size %d\n", *startSentinel, size);
        }
        // change the sentinel values to free
        if(print) {
            printf("start and end before: %d %d\n", *reinterpret_cast<int*>(startSentinel), *reinterpret_cast<int*>(endSentinel));
        }
        int* ss = reinterpret_cast<int*>(startSentinel);
        int* es = reinterpret_cast<int*>(endSentinel);
        *ss *= -1;
        *es *= -1;
        if(print) {
            printf("start and end after: %d %d\n", *reinterpret_cast<int*>(startSentinel), *reinterpret_cast<int*>(endSentinel));
        }
        if(print) {
            printf(" %d == %d? %d == %d?\n", *ss, *reinterpret_cast<int*>(startSentinel), *es, *reinterpret_cast<int*>(endSentinel));
        }
        // check the previous sentinel to see if we need to merge
        if(ss > begin()._p) {
            if(print) {
                printf("this isn't the first block\n");
            }
            if(*(ss - 1) > 0) {
                if(print) {
                    printf("merge with previous block\n");
                }
                // merge the current block with the previous block
                char* beforeStartSentinel = startSentinel - 4 - *reinterpret_cast<int*>(startSentinel - 4) - 4;
                int* bss = reinterpret_cast<int*>(beforeStartSentinel);
                *bss += 8 + *ss;
                *es = *bss;
                ss = bss;
            } else {
                if(print) {
                    printf("the previous block is allocated; no need to merge\n");
                }
            }
        }
        // check the next sentinel to see if we need to merge
        if(es + 1 < end()._p) {
            if(print) {
                printf("this isn't the last block\n");
            }
            if(*(es + 1) > 0) {
                if(print) {
                    printf("merge with next block\n");
                }
                // merge the current block with the next block
                char* afterEndSentinel = endSentinel + 8 + *(es + 1);
                int* aes = reinterpret_cast<int*>(afterEndSentinel);
                *aes += 8 + *es;
                *ss = *aes;
            } else {
                if(print) {
                    printf("the next block is allocated; no need to merge\n");
                }
            }
        }
        bool isValid = valid();
        if(print) {
            printf("--- deallocate %d---\n", isValid);
        }
        assert(isValid);
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        // bool isValid = valid();
        // printf("---destroy %d---\n", isValid);
        // assert(isValid);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
