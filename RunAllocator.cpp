// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    const value_type v = 8;
    bool debug = false;
    if (debug) {
        printf("RUN HARNESS: value_type size = %d\n", sizeof(value_type));
    }
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    // cout << "-40 944"     << endl;
    // cout << "-40 -24 912" << endl;
    // cout << "40 -24 912"  << endl;
    // cout << "72 -24 880"  << endl;
    if (debug) {
        printf("beginning tests\n");
    }
    int times;
    string input;
    getline(cin, input);
    stringstream inTimes(input);
    inTimes >> times;
    getline(cin, input);
    if (debug) {
        printf("asserting that next line was empty\n");
    }
    assert(input.empty()); // ensures we're in an empty line
    if (debug) {
        printf("still good to go\n");
    }
    for(int i = 0; i < times; ++i) {
        allocator_type allocator;
        // vector<value_type*> allocatedSpots;
        // vector<int> allocatedNums;
        if (debug) {
            printf("\nTEST CASE #%d\n", i + 1);
        }
        getline(cin, input); // should read 5; segfaults here
        if (debug) {
            printf("checking input\n");
        }
        while(!input.empty()) {
            // interpret the next request as either an allocation or deallocation
            if (debug) {
                printf("input not empty\n");
            }
            int currNum;
            istringstream inCurrNum(input);
            inCurrNum >> currNum;
            if(currNum > 0) {
                if (debug) {
                    printf("allocate %d bytes\n", currNum * 8   );
                }
                allocator.allocate(currNum);
                // allocatedSpots.push_back(allocator.allocate(currNum));
                // allocatedNums.push_back(currNum);
            } else {
                if (debug) {
                    printf("deallocate busy block %d\n", currNum * -1);
                }
                int n = currNum * -1;
                allocator_type::iterator iter = allocator.begin();
                // iterate through the blocks and find the nth occupied block
                while(n > 0) {
                    if(*iter < 0) {
                        --n;
                    }
                    if(n != 0) {
                        ++iter;
                    }
                }
                if (debug) {
                    printf("start of memory: %d\n", allocator.begin()._p);
                }
                if (debug) {
                    printf("deallocating location %d with size %d\n", reinterpret_cast<value_type*>(iter._p + 1), abs(*iter._p)/sizeof(value_type));
                }
                allocator.deallocate(reinterpret_cast<value_type*>(iter._p + 1), abs(*iter._p)/sizeof(value_type));
                // allocator.deallocate(allocatedSpots.at((currNum * -1) - 1), allocatedNums.at((currNum * -1) - 1));
                // allocatedSpots.erase(allocatedSpots.begin() + (currNum * -1) - 1);
                // allocatedNums.erase(allocatedNums.begin() + (currNum * -1) - 1);
                if (debug) {
                    printf("returned from deallocation\n");
                }
            }
            getline(cin, input);
            if(debug) {
                printf("here's where we are right now\n");
                allocator_type::iterator iter = allocator.begin();
                int size = std::abs(*iter);
                int max = 1000;
                while(size < max) {
                    cout << *iter;
                    ++iter;
                    max -= 8;
                    size += std::abs(*iter);
                    if(size < max) {
                        cout << " ";
                    }
                }
                cout << "\n";
            }
        }
        if(debug) {
            printf("input empty...\n");
        }
        allocator_type::iterator iter = allocator.begin();
        int size = std::abs(*iter);
        int max = 1000;
        while(size < max) {
            cout << *iter;
            ++iter;
            max -= 8;
            size += std::abs(*iter);
            if(size < max) {
                cout << " ";
            }
        }
        cout << "\n";
    }

    return 0;
}
