// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

// debugging method that prints the current contents of an allocator
void print(my_allocator<double, 1000> x) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    printf("\nCURRENT STATE:\n");
    allocator_type::iterator iter = x.begin();
    while(iter._p < x.end()._p) {
        printf("%d ", *iter);
        ++iter;
    }
    printf("\n\n");
}

// tests the iterator == method
TEST(AllocatorFixture, iterator_equals) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    allocator_type::iterator a = x.begin();
    allocator_type::iterator b = x.begin();
    ASSERT_EQ(a, b);
}

// tests the iterator == method after ++ operations
TEST(AllocatorFixture, iterator_equals2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    const pointer    p2 = x.allocate(s);
    allocator_type::iterator a = x.begin();
    allocator_type::iterator b = x.begin();
    ++a;
    ++b;
    ASSERT_EQ(a, b);
}

// tests the iterator == method after ++ and -- operations
TEST(AllocatorFixture, iterator_equals3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    const pointer    p2 = x.allocate(s);
    allocator_type::iterator a = x.begin();
    allocator_type::iterator b = x.begin();
    ++a;
    ++b;
    --a;
    --b;
    ASSERT_EQ(a, b);
}

// tests the iterator ++ method
TEST(AllocatorFixture, iterator_plusplus) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    const pointer    p2 = x.allocate(s);
    const pointer    p3 = x.allocate(s);
    const pointer    p4 = x.allocate(s);
    const pointer    p5 = x.allocate(s);
    const pointer    p6 = x.allocate(s);
    const pointer    p7 = x.allocate(s);
    const pointer    p8 = x.allocate(s);
    const pointer    p9 = x.allocate(s);
    allocator_type::iterator a = x.begin();
    allocator_type::iterator b = x.begin();
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ASSERT_EQ(a, b);
}

// tests the iterator -- method
TEST(AllocatorFixture, iterator_minusminus) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    const pointer    p2 = x.allocate(s);
    const pointer    p3 = x.allocate(s);
    const pointer    p4 = x.allocate(s);
    const pointer    p5 = x.allocate(s);
    const pointer    p6 = x.allocate(s);
    const pointer    p7 = x.allocate(s);
    const pointer    p8 = x.allocate(s);
    const pointer    p9 = x.allocate(s);
    allocator_type::iterator a = x.begin();
    allocator_type::iterator b = x.begin();
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++a;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    ++b;
    --a;
    --a;
    --a;
    --a;
    --a;
    --a;
    --a;
    --a;
    --a;
    --b;
    --b;
    --b;
    --b;
    --b;
    --b;
    --b;
    --b;
    --b;
    ASSERT_EQ(a, b);
}

// tests the const_iterator == method
TEST(AllocatorFixture, const_iterator_equals) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    const allocator_type x;
    const size_type  s = 8;
    allocator_type::const_iterator a = x.begin();
    allocator_type::const_iterator b = x.begin();
    ASSERT_EQ(a, b);
}

// tests that no two allocators will evaluate to true
TEST(AllocatorFixture, allocator_equals) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    allocator_type   y;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    const pointer    p2 = y.allocate(s);
    ASSERT_NE(x, y);
}

// tests that even an allocator compared to itself will not eval to true
TEST(AllocatorFixture, allocator_equals2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    ASSERT_NE(x, x);
}

// test that defines the behavior of c's allocator. Tests construct, allocate, destroy, and deallocate.
TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// test that defines the behavior of my allocator. Tests construct, allocate, destroy, and deallocate.
TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    printf("TEST1: b is %d\n", b);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    else {
        printf("TEST1: b is nullptr; not good!\n");
    }
}

// makes sure nearly all space can be allocated without issue
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const value_type v = 2;

    const size_type s0 = 7;
    const pointer b0 = x.allocate(s0);
    print(x);

    const size_type s1 = 49;
    const pointer b1 = x.allocate(s1);
    print(x);

    x.deallocate(b1, s1);
    print(x);

    x.deallocate(b0, s0);
    print(x);

    const size_type s2 = 81;
    const pointer b2 = x.allocate(s2);
    print(x);

    x.deallocate(b2, s2);
    print(x);

    const size_type s3 = 43;
    const pointer b3 = x.allocate(s3);
    print(x);

    const size_type s4 = 30;
    const pointer b4 = x.allocate(s4);
    print(x);

    const size_type s5 = 20;
    const pointer b5 = x.allocate(s5);
    print(x);

    const size_type s6 = 14;
    const pointer b6 = x.allocate(s6);
    print(x);

    x.deallocate(b3, s3);
    print(x);

    const size_type s7 = 26;
    const pointer b7 = x.allocate(s7);
    print(x);

    const size_type s8 = 14;
    const pointer b8 = x.allocate(s8);
    print(x);

    const size_type s9 = 9;
    const pointer b9 = x.allocate(s9);
    print(x);

    const size_type s10 = 2;
    const pointer b10 = x.allocate(s10);
    print(x);

    ASSERT_EQ(true, true);
}

// makes sure all space can be allocated without issue
TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const value_type v = 2;

    const size_type s0 = 7;
    const pointer b0 = x.allocate(s0);
    print(x);

    const size_type s1 = 49;
    const pointer b1 = x.allocate(s1);
    print(x);

    x.deallocate(b1, s1);
    print(x);

    x.deallocate(b0, s0);
    print(x);

    const size_type s2 = 81;
    const pointer b2 = x.allocate(s2);
    print(x);

    x.deallocate(b2, s2);
    print(x);

    const size_type s3 = 43;
    const pointer b3 = x.allocate(s3);
    print(x);

    const size_type s4 = 30;
    const pointer b4 = x.allocate(s4);
    print(x);

    const size_type s5 = 20;
    const pointer b5 = x.allocate(s5);
    print(x);

    const size_type s6 = 14;
    const pointer b6 = x.allocate(s6);
    print(x);

    x.deallocate(b3, s3);
    print(x);

    const size_type s7 = 26;
    const pointer b7 = x.allocate(s7);
    print(x);

    const size_type s8 = 14;
    const pointer b8 = x.allocate(s8);
    print(x);

    const size_type s9 = 9;
    const pointer b9 = x.allocate(s9);
    print(x);

    const size_type s10 = 2;
    const pointer b10 = x.allocate(s10);
    print(x);

    const size_type s11 = 1;
    const pointer b11 = x.allocate(s11);
    print(x);

    ASSERT_EQ(true, true);
}

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test

// tests that begin will return at the start of the stored array
TEST(AllocatorFixture, begin) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    allocator_type::iterator iter = x.begin();
    ASSERT_EQ(x[0], *iter);
}

// tests that begin will return at the start of the stored array
TEST(AllocatorFixture, end) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    allocator_type::iterator iter = x.end();
    ASSERT_EQ(x[1000], *iter);
}

// tests that begin will return at the start of the stored array for const allocators
TEST(AllocatorFixture, const_begin) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    const allocator_type   x;
    const size_type  s = 8;
    allocator_type::const_iterator iter = x.begin();
    ASSERT_EQ(x[0], *iter);
}

// tests that begin will return at the start of the stored array for const allocators
TEST(AllocatorFixture, const_end) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    const allocator_type   x;
    const size_type  s = 8;
    allocator_type::const_iterator iter = x.end();
    ASSERT_EQ(x[1000], *iter);
}

// tests that [] works correctly on allocators for ints
TEST(AllocatorFixture, bracketsInt) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    ASSERT_EQ(x[0], -32);
}

// tests that [] works correctly on allocators for doubles
TEST(AllocatorFixture, bracketsDouble) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 8;
    const pointer    p = x.allocate(s);
    ASSERT_EQ(x[0], -64);
}